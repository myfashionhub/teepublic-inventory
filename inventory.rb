require 'json'

class Inventory
	attr_accessor :products
	attr_reader   :error

	def initialize
		@products = {}
		@error = nil
	end

	def validate_args(args)
		if args.size == 0
			@error = 'Must provide product_type'
			return
		end

		product_type = args[0]
		input_values = args[1..-1]

		[product_type, input_values]
	end

	def get_products_from_file(filename)
		begin
			file = File.read(filename)
			data = JSON.parse(file)
		rescue => e
			@error = "Error reading file: #{e.to_s}"
		end

		return if data.nil?

		data.each do |product_hash|
			product_type = product_hash['product_type']
			@products[product_type] = @products[product_type] || []
			@products[product_type].push(product_hash['options'])
		end
	end

	def get_non_inputted_options(product_type, input_values)
		result = {}
		total_matches = 0

		@products[product_type].each do |product_options|
			# Check if any inputted option doesn't match the current product
			# Skip to next product if this is the case
			option_matches = input_values.map do |value|
				product_options.values.include?(value)
			end
			if option_matches.include?(false)
				next
			end

			# Keep track of number of matches to distinguish between
			# non-match and exact match
			total_matches += 1

			# Get the non-inputted values for the result
			product_options.each do |key, value|
				if !input_values.include?(value)
					result[key] = result[key] || []
					result[key].push(value)
				end
			end
		end

		result.each do |key, values|
			result[key] = values.uniq
		end

		[total_matches, result]
	end

	def get_output(total_matches, result)
		if total_matches == 0
			return 'The product with specified options does not exist'
		end

		if result.keys.size == 0
			return 'The specified product exists in inventory'
		end

		output = ''
		result.each do |key, values|
			output += "#{key.capitalize}: #{values.join(', ')}\n"
		end

		output
	end
end
