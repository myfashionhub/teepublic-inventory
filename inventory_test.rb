require 'test/unit'

require_relative 'inventory'

class InventoryTest < Test::Unit::TestCase
	def setup
		@parser = Inventory.new
		@sample_products = {
			'tshirt' => [
				{
					'gender' => 'male',
					'color' => 'red',
					'size' => 'small',
				},
				{
					'gender' => 'female',
					'color' => 'red',
					'size' => 'small',
				},
				{
					'gender' => 'female',
					'color' => 'blue',
					'size' => 'medium',
				},
				{
					'gender' => 'female',
					'color' => 'red',
					'size' => 'large',
				},
			],
		}
	end

	def test_initialize
		assert_equal(@parser.products, {})
		assert_equal(@parser.error, nil)
	end

	def test_invalid_args
		@parser.validate_args([])
		assert_equal(@parser.error, 'Must provide product_type')
	end

	def test_valid_args
		@parser.validate_args(['product_type', 'option1', 'option2'])
		assert_equal(@parser.error, nil)
	end

	def test_error_reading_file
		@parser.get_products_from_file('random.json')
		assert_equal(@parser.error, 'Error reading file: No such file or directory @ rb_sysopen - random.json')
	end

	def test_get_products_from_file
		@parser.get_products_from_file('products.json')
		assert_equal(@parser.products.size, 3)
		assert_equal(@parser.products.keys, ['tshirt', 'mug', 'sticker'])
		assert_equal(@parser.products['tshirt'].size, 50)
		assert_equal(@parser.products['mug'].size, 2)
		assert_equal(@parser.products['sticker'].size, 8)
	end

	def test_product_not_present
		@parser.products = @sample_products
		total_matches, result = @parser.get_non_inputted_options('tshirt', ['male', 'blue'])
		assert_equal(total_matches, 0)
	end

	def test_get_non_inputted_options
		@parser.products = @sample_products
		total_matches, result = @parser.get_non_inputted_options('tshirt', ['female', 'red'])
		assert_equal(result, {'size' => ['small', 'large']})
	end

	def test_get_output
		output = @parser.get_output(0, {})
		assert_equal(output, 'The product with specified options does not exist')

		output = @parser.get_output(4, {})
		assert_equal(output, 'The specified product exists in inventory')

		result = {'color': ['blue', 'white'], 'size': ['small', 'large']}
		output = @parser.get_output(4, result)
		assert_equal(output, "Color: blue, white\nSize: small, large\n")
	end
end
