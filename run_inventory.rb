#!/usr/bin/env ruby

require_relative 'inventory'

def run(filename, args)
	parser = Inventory.new

	product_type, input_values = parser.validate_args(args)
	if !parser.error.nil?
		puts parser.error
		return
	end

	parser.get_products_from_file(filename)
	if !parser.error.nil?
		puts parser.error
		return
	end

	option_matches, result = parser.get_non_inputted_options(product_type, input_values)
	puts parser.get_output(option_matches, result)
end


run('products.json', ARGV)
