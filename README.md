# Inventory parser

[Prompt](https://gist.github.com/JoeMcB/72ea691c406240889fb4ec165dd8c7f0)

This is a command line program:
```
$ ./run_inventory.rb tshirt male navy
```

To run test suite
```
$ ruby inventory_test.rb
```
